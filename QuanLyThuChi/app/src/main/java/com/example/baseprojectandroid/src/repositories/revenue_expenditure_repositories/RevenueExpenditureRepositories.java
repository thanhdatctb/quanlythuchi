package com.example.baseprojectandroid.src.repositories.revenue_expenditure_repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.baseprojectandroid.async.DeleteEvenueExpenditureAsyncTask;
import com.example.baseprojectandroid.async.InsertEvenueExpenditureAsyncTask;
import com.example.baseprojectandroid.async.UpdateEvenueExpenditureAsyncTask;
import com.example.baseprojectandroid.cores.room.dao.EevenueExpenditureDao;
import com.example.baseprojectandroid.cores.room.database.AppDatabase;
import com.example.baseprojectandroid.cores.room.table.RevenueExpenditureTable;
import com.example.baseprojectandroid.models.spinner_model.SpinnerModel;

import java.util.ArrayList;
import java.util.List;

public class RevenueExpenditureRepositories {
    private List<SpinnerModel> mListSpinerRevenue = new ArrayList<>();
    private List<SpinnerModel> mListSpinerExpenses = new ArrayList<>();
    private EevenueExpenditureDao mEvenueExpenditureDao;

    public RevenueExpenditureRepositories(Application application) {
        mEvenueExpenditureDao = AppDatabase.getInstance(application).taskEvenueExpenditureDao();
    }

    //async dao
    public void insert(RevenueExpenditureTable revenueExpenditureTable) {
        new InsertEvenueExpenditureAsyncTask(revenueExpenditureTable, mEvenueExpenditureDao).execute();
    }

    public void delete(RevenueExpenditureTable revenueExpenditureTable) {
        new DeleteEvenueExpenditureAsyncTask(revenueExpenditureTable, mEvenueExpenditureDao).execute();
    }

    public void update(RevenueExpenditureTable revenueExpenditureTable) {
        new UpdateEvenueExpenditureAsyncTask(revenueExpenditureTable, mEvenueExpenditureDao).execute();
    }

    public LiveData<List<RevenueExpenditureTable>> getListEevenueExpenditure(String type) {
        return mEvenueExpenditureDao.getListRevenueExpenditure(type);
    }

    public LiveData<List<RevenueExpenditureTable>>getmListAllEevenueExpenditure(){
        return mEvenueExpenditureDao.getAllListRevenueExpenditure();
    }

    //get list spinner
    public LiveData<List<SpinnerModel>> getListSpinnerRevenue() {
        MutableLiveData<List<SpinnerModel>> listTmt = new MutableLiveData<>();
        mListSpinerRevenue.clear();
        mListSpinerRevenue.add(new SpinnerModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS10aPi7W9xsHqOq2g3orJHs-9DaRv9UgQYlQ&usqp=CAU", "Khác"));
        mListSpinerRevenue.add(new SpinnerModel("https://e7.pngegg.com/pngimages/1004/46/png-clipart-real-estate-building-property-house-business-office-building-angle-building.png", "Cho thuê nhà"));
        mListSpinerRevenue.add(new SpinnerModel("https://www.vhv.rs/dpng/d/440-4409798_gift-vector-icon-icon-gift-vector-png-transparent.png", "Quà tặng"));
        mListSpinerRevenue.add(new SpinnerModel("https://e7.pngegg.com/pngimages/396/516/png-clipart-computer-icons-commerce-discount-icon-logo-desktop-wallpaper.png", "Trợ cấp"));
        mListSpinerRevenue.add(new SpinnerModel("https://banner2.cleanpng.com/20190424/skb/kisspng-computer-icons-portable-network-graphics-clip-art-worker-png-icon-27-png-repo-free-png-icons-5cc116fecd7204.7681226515561582068415.jpg", "Việc làm"));
        mListSpinerRevenue.add(new SpinnerModel("https://img95.699pic.com/element/40137/9272.png_300.png", "Buôn bán"));
        mListSpinerRevenue.add(new SpinnerModel("https://www.pngfind.com/pngs/m/140-1407971_back-payment-payment-icon-png-transparent-png-download.png", "Thu nhập chính"));
        mListSpinerRevenue.add(new SpinnerModel("https://cdn.iconscout.com/icon/premium/png-512-thumb/money-raise-986677.png", "Lãi suất"));
        listTmt.setValue(mListSpinerRevenue);
        return listTmt;
    }

    public LiveData<List<SpinnerModel>>getListSpinnerExpenses(){
        MutableLiveData<List<SpinnerModel>> listTmt = new MutableLiveData<>();
        mListSpinerExpenses.clear();
        mListSpinerExpenses.add(new SpinnerModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS10aPi7W9xsHqOq2g3orJHs-9DaRv9UgQYlQ&usqp=CAU", "Khác"));
        mListSpinerExpenses.add(new SpinnerModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRq5nk0iXxTzz6J4FRnosDkgOxU45rglMkS5w&usqp=CAU", "Gia đình"));
        mListSpinerExpenses.add(new SpinnerModel("https://e7.pngegg.com/pngimages/160/546/png-clipart-shopping-cart-computer-icons-online-shopping-retail-shopping-icon-angle-text.png", "Mua sắm"));
        mListSpinerExpenses.add(new SpinnerModel("https://image.flaticon.com/icons/png/512/105/105190.png", "Ăn uống"));
        mListSpinerExpenses.add(new SpinnerModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvE5DskTF9umy-JLYWjc7TqnHak9VdIMih-w&usqp=CAU", "Điện nước"));
        mListSpinerExpenses.add(new SpinnerModel("https://i.vippng.com/png/small/98-988945_png-file-svg-icon-chm-sc-sc-khe.png", "Sức khoẻ"));
        mListSpinerExpenses.add(new SpinnerModel("https://www.pngfind.com/pngs/m/140-1407971_back-payment-payment-icon-png-transparent-png-download.png", "Cho vay"));
        mListSpinerExpenses.add(new SpinnerModel("https://www.grap.io/files/8a8ed085350fe583f00a2e394ffdea8a34a20efe7201193062a22599f4ca4b1f.png", "Grap,Uber"));
        mListSpinerExpenses.add(new SpinnerModel("https://play-lh.googleusercontent.com/LMmgvfK1xrfmN5Ak0g4-YZnnaYjslFsoR-T_eFs_5cMGyWZXuQ_E5SZnbme8G82rLAc", "Xe bus"));
        mListSpinerExpenses.add(new SpinnerModel("https://e7.pngegg.com/pngimages/926/425/png-clipart-airplane-computer-icons-symbol-airport-hand-logo-thumbnail.png", "Vé máy bay"));
        mListSpinerExpenses.add(new SpinnerModel("https://ezstatic1.ezweb.online/portal/blog/khoi-nghiep-kinh-doanh/ban-trai-cay-de-kiem-duoc-loi-nhieu/ban-trai-cay-de-kiem-duoc-loi-nhieu-1.png", "Kinh doanh"));
        listTmt.setValue(mListSpinerExpenses);
        return listTmt;
    }

    //dữ liệu của năm
    public LiveData<List<String>>getListYear(){
        MutableLiveData<List<String>>listMutableLiveData = new MutableLiveData<>();
        List<String> list = new ArrayList<>();
        for (int i = 16; i <= 30 ; i++){
            list.add("20"+i);
        }
        listMutableLiveData.setValue(list);
        return listMutableLiveData;
    }

    public LiveData<List<String>>getListMounth(){
        MutableLiveData<List<String>>listMutableLiveData = new MutableLiveData<>();
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 12 ; i++){
            list.add(i+"");
        }
        listMutableLiveData.setValue(list);
        return listMutableLiveData;
    }
}
